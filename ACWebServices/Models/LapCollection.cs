﻿using ACWebServices.DAL;
using DAL;
using System;
using System.Collections.Generic;
using System.Data;

namespace ACWebServices.Models
{
    public class LapCollection
    {
        public LapCollection()
        {

        }

        public LapCollection(int id)
        {
            var ds = Select.Laps(id);

            var fastestLapsDT = ds.Tables[0];
            var lapsDT = ds.Tables[1];

            Laps = new List<Lap>();

            foreach (DataRow dr in fastestLapsDT.Rows)
            {
                var lap = new Lap(dr.GetValue<string>(nameof(Lap.Driver)), dr.GetValue<int>(nameof(Lap.LapTime)), dr.GetValue<DateTime>(nameof(Lap.DateStamp)));

                lap.Laps = new List<Lap>();

                foreach (DataRow odr in lapsDT.Select($"driver = '{lap.Driver}'"))
                {
                    lap.Laps.Add(new Lap(odr.GetValue<string>(nameof(Lap.Driver)), odr.GetValue<int>(nameof(Lap.LapTime)), odr.GetValue<DateTime>(nameof(Lap.DateStamp))));
                }

                Laps.Add(lap);
            }

        }

        public List<Lap> Laps { get; set; }
    }
}