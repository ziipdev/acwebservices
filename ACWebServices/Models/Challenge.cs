﻿using System;

namespace ACWebServices.Models
{
    public class Challenge
    {
        public int ID { get; set; }
        public DateTime StartDate { get; set; }
        public string Car { get; set; }
        public string Track { get; set; }
    }
}