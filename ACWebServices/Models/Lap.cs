﻿using System;
using System.Collections.Generic;

namespace ACWebServices.Models
{
    public class Lap
    {
        public Lap(){}

        public Lap(string driver, int time, DateTime datestamp)
        {
            SetLapTime(time);
            Driver = driver;
            DateStamp = datestamp;
        }

        public DateTime DateStamp { get; set; }
        public string Driver { get; set; }
        public int LapTime { get; set; }
        public string DisplayTime { get; set; }

        public List<Lap> Laps { get; set; }

        private void SetLapTime(int time)
        {
            LapTime = time;

            int m = 0;
            int s = 0;
            int p = time;

            if (p >= 60000)
            {
                m = p / 60000;
                p -= (m * 60000);
            }

            if (p >= 1000)
            {
                s = p / 1000;
                p -= (s * 1000);
            }

            DisplayTime = $"{m.ToString("00")}:{s.ToString("00")}.{p.ToString("000")}";
        }
    }
}