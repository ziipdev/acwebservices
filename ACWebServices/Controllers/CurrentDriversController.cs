﻿using Newtonsoft.Json;
using System.Web.Http;

namespace ACWebServices.Controllers
{
    public class CurrentDriversController : ApiController
    {
        public IHttpActionResult GetCurrentDrivers(bool test = false)
        {
            if(test) return Json(JsonConvert.DeserializeObject("['Bevan Johnson','DarkJester']"));

            return Json(JsonConvert.DeserializeObject(DAL.Select.CurrentDrivers()));
        }
    }
}
