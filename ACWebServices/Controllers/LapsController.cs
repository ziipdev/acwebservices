﻿using ACWebServices.Models;
using System.Web.Http;

namespace ACWebServices.Controllers
{
    public class LapsController : ApiController
    {
        public IHttpActionResult GetLaps(int id)
        {
            return Ok(new LapCollection(id).Laps);
        }
    }
}
