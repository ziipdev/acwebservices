﻿using ACWebServices.DAL;
using ACWebServices.Models;
using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Http;

namespace ACWebServices.Controllers
{
    public class ChallengesController : ApiController
    {
        public IHttpActionResult GetChallenges()
        {
            var dt = Select.Challenges();
            var challenges = new List<Challenge>();

            foreach (DataRow dr in dt.Rows)
            {
                challenges.Add(new Challenge()
                {
                    ID = dr.GetValue<int>(nameof(Challenge.ID)),
                    StartDate = dr.GetValue<DateTime>(nameof(Challenge.StartDate)),
                    Car = dr.GetValue<string>(nameof(Challenge.Car)),
                    Track = dr.GetValue<string>(nameof(Challenge.Track))
                });
            }

            return Ok(challenges);
        }
    }
}
