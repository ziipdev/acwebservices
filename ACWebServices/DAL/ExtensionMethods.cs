﻿using System;
using System.Data;

namespace DAL
{
    /// <summary>
    /// Extension methods related to working with Sql data.
    /// </summary>
    public static class ExtensionMethods
    {
        /// <summary>
        /// Gets the value from a DataRow and converts it to the specified enum type, if the column is not found then the default for the type is returned.
        /// Note: This is capable of both string and integer parsing to enum value. If a column is found but it has no data, the fallback will be used.
        /// </summary>
        /// <typeparam name="T">Must be an enum type</typeparam>
        /// <param name="record">The data row record</param>
        /// <param name="name">The name of the column in the data row</param>
        /// <param name="ignoreCase">Do/do not ignore case when parsing the enum</param>
        /// <param name="fallBackValue">The enum value that should be used if the column cannot be found in the DataRow. Suggested syntax - nameof(Enum.MemberName).</param>
        /// <returns>A valid value for the given type</returns>
        public static T GetValueEnum<T>(this DataRow record, string name, bool ignoreCase, string fallBackValue) where T : Enum
        {
            var str = !record.Table.Columns.Contains(name) 
                ? fallBackValue : Convert.ToString(record[name]);

            if (string.IsNullOrEmpty(str))
                str = fallBackValue;
                
            return (T)Enum.Parse(typeof(T), str, ignoreCase);
        }

        /// <summary>
        /// Gets the value from a DataRow and converts it to the specified type, if the column is not found the default for the type is returned. 
        /// </summary>
        /// <param name="record">The data row record</param>
        /// <param name="name">The name of the column in the data row</param>
        /// <param name="fallBackObject">If the column cannot be found, return this instead</param>
        /// <returns>A valid value for the given type</returns>
        public static T GetValue<T>(this DataRow record, string name, object fallBackObject = null)
        {
            // Doesn't exist, return default for type
            if (!record.Table.Columns.Contains(name))
            {
                // If it was null, return fallBackObject specified
                if (fallBackObject != null)
                    return (T)fallBackObject;

                return default;
            }

            // Successful
            if (!Convert.IsDBNull(record[name]))
                return (T)record[name];

            // Final failure, return default for type
            return default;
        }

        /// <summary>
        /// Determines if the DataTable contains any rows.
        /// </summary>
        public static bool Any(this DataTable dt)
        {
            return dt.Rows.Count > 0;
        }

        /// <summary>
        /// Determines if the DataTable contains only one row
        /// </summary>
        public static bool Single(this DataTable dt)
        {
            return dt.Rows.Count == 1;
        }

        /// <summary>
        /// Returns the first row in the DataTable, if the number of rows is not 1, null is returned.
        /// </summary>
        public static DataRow First(this DataTable dt)
        {
            return dt.Single() ? dt.Rows[0] : null;
        }
    }
}
