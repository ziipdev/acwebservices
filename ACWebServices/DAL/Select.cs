﻿using DAL;
using System.Configuration;
using System.Data;

namespace ACWebServices.DAL
{
    public static class Select
    {
        public static DataTable Challenges()
        {
            return SqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["SQL"].ConnectionString, CommandType.StoredProcedure, "Challenges_List").Tables[0];
        }

        public static string CurrentDrivers()
        {
            return SqlHelper.ExecuteScalar(ConfigurationManager.ConnectionStrings["SQL"].ConnectionString, CommandType.StoredProcedure, "CurrentDrivers_List").ToString();
        }

        public static DataSet Laps(int id)
        {
            return SqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["SQL"].ConnectionString, CommandType.StoredProcedure, "Laps_List", SqlHelper.SetParameter("id",SqlDbType.Int,id));
        }
    }
}