﻿using Swagger.Net.Application;
using System.Web.Http;

namespace ACWebServices
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
    name: "CurrentDriversApi",
    routeTemplate: "api/currentdrivers/{test}",
    defaults: new { test = RouteParameter.Optional, controller= "currentdrivers" }
);

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );


        }
    }
}
